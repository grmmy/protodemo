package girmmy.org.protodemo.models.helpers;

import girmmy.org.protodemo.models.proto.PersonOuterClass;

/**
 * Created by Anton Minashkin on 11/21/14.
 */
public class ProtoHelper {

    private void readFromProtoPerson(PersonOuterClass.Person person) {
        final Integer id = person.getId();
        final String number = person.getPhoneList().get(0).getNumber();
        final String name = person.getName();
        final int size = person.getPhoneList().size();
    }

    private void createNewProtoPerson() {
        PersonOuterClass.Person newPerson = PersonOuterClass.Person.newBuilder()
                .setName("New Name")
                .setEmail("New Email")
                .setId(0)
                .addPhone(PersonOuterClass.Person.PhoneNumber.newBuilder()
                        .setNumber("333-444-555")
                        .setType(PersonOuterClass.Person.PhoneType.HOME)
                        .build())
                .build();
    }
}
