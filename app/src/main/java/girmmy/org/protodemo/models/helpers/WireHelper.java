package girmmy.org.protodemo.models.helpers;

import java.util.ArrayList;
import java.util.List;

import girmmy.org.protodemo.models.wire.Person;

/**
 * Created by Anton Minashkin on 11/21/14.
 */
public class WireHelper {
    private void readFromWirePerson(Person person) {
        final String email = person.email;
        final Integer id = person.id;
        final String number = person.phone.get(0).number;
        final String name = person.name;
        final int size = person.phone.size();
    }

    private void createNewWirePerson() {
        List<Person.PhoneNumber> numbers = new ArrayList<Person.PhoneNumber>();
        numbers.add(new Person.PhoneNumber("333-444", Person.PhoneType.HOME));
        numbers.add(new Person.PhoneNumber("333-445", Person.PhoneType.MOBILE));
        Person newPerson = new Person("Name", 1, "New Email", numbers);
    }
}
