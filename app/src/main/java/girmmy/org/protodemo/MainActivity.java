package girmmy.org.protodemo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import girmmy.org.protodemo.api.DemoApi;
import girmmy.org.protodemo.models.proto.PersonOuterClass;
import girmmy.org.protodemo.models.wire.Person;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.ProtoConverter;
import retrofit.converter.WireConverter;

import static girmmy.org.protodemo.models.wire.Person.*;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getProtoModel();
//        getWireModel();
    }

    private void getProtoModel() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://10.0.2.2:8080")
                .setConverter(new ProtoConverter())
                .build();
        final DemoApi demoApi = restAdapter.create(DemoApi.class);
        final TextView text = (TextView) findViewById(R.id.text);
        demoApi.getProtoPerson(new Callback<PersonOuterClass.Person>() {
            @Override
            public void success(PersonOuterClass.Person person, Response response) {
                text.setText(person.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Demo", "Something goes wrong");
            }
        });
    }



    private void getWireModel() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://10.0.2.2:8080")
                .setConverter(new WireConverter())
                .build();
        final DemoApi demoApi = restAdapter.create(DemoApi.class);
        final TextView text = (TextView) findViewById(R.id.text);
        demoApi.getWirePerson(new Callback<Person>() {
            @Override
            public void success(Person person, Response response) {
                text.setText(person.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Demo", "Something goes wrong");
            }
        });
    }


}
