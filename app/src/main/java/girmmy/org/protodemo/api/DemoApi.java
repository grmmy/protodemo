package girmmy.org.protodemo.api;

import girmmy.org.protodemo.models.proto.PersonOuterClass;
import girmmy.org.protodemo.models.wire.Person;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Anton Minashkin on 11/20/14.
 */
public interface DemoApi {
    @GET("/")
    void getProtoPerson(Callback<PersonOuterClass.Person> callback);

    @GET("/")
    void getWirePerson(Callback<Person> callback);
}
